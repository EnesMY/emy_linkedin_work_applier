﻿namespace LinkedIn_Applier
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtWorkType = new System.Windows.Forms.TextBox();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lstCountries = new System.Windows.Forms.ListBox();
            this.btnDeleteCountry = new System.Windows.Forms.Button();
            this.btnAddCountry = new System.Windows.Forms.Button();
            this.cbOutomaticSendEmail = new System.Windows.Forms.CheckBox();
            this.btnIndent = new System.Windows.Forms.Button();
            this.btnIndentRemove = new System.Windows.Forms.Button();
            this.btnRedo = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnSelectColor = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.btnUnderline = new System.Windows.Forms.Button();
            this.btnItalic = new System.Windows.Forms.Button();
            this.btnBold = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAttachmentFile = new System.Windows.Forms.Label();
            this.btmOpenFile = new System.Windows.Forms.Button();
            this.txtFileLocation = new System.Windows.Forms.TextBox();
            this.rtbMessage = new System.Windows.Forms.RichTextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.grpEmailDetails = new System.Windows.Forms.GroupBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.cbHideEverything = new System.Windows.Forms.CheckBox();
            this.grpEmailDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtWorkType
            // 
            this.txtWorkType.Location = new System.Drawing.Point(10, 176);
            this.txtWorkType.Name = "txtWorkType";
            this.txtWorkType.Size = new System.Drawing.Size(417, 20);
            this.txtWorkType.TabIndex = 0;
            this.txtWorkType.Text = "C#";
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(12, 126);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(215, 20);
            this.txtLocation.TabIndex = 3;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(12, 239);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(415, 28);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Step 1 - Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Work Keywords";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Location";
            // 
            // lstCountries
            // 
            this.lstCountries.FormattingEnabled = true;
            this.lstCountries.Location = new System.Drawing.Point(12, 22);
            this.lstCountries.Name = "lstCountries";
            this.lstCountries.Size = new System.Drawing.Size(415, 95);
            this.lstCountries.TabIndex = 10;
            // 
            // btnDeleteCountry
            // 
            this.btnDeleteCountry.Location = new System.Drawing.Point(333, 123);
            this.btnDeleteCountry.Name = "btnDeleteCountry";
            this.btnDeleteCountry.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteCountry.TabIndex = 11;
            this.btnDeleteCountry.Text = "Delete";
            this.btnDeleteCountry.UseVisualStyleBackColor = true;
            this.btnDeleteCountry.Click += new System.EventHandler(this.BtnDeleteCountry_Click);
            // 
            // btnAddCountry
            // 
            this.btnAddCountry.Location = new System.Drawing.Point(233, 123);
            this.btnAddCountry.Name = "btnAddCountry";
            this.btnAddCountry.Size = new System.Drawing.Size(94, 23);
            this.btnAddCountry.TabIndex = 12;
            this.btnAddCountry.Text = "Add";
            this.btnAddCountry.UseVisualStyleBackColor = true;
            this.btnAddCountry.Click += new System.EventHandler(this.btnAddCountry_Click);
            // 
            // cbOutomaticSendEmail
            // 
            this.cbOutomaticSendEmail.AutoSize = true;
            this.cbOutomaticSendEmail.Location = new System.Drawing.Point(3, 0);
            this.cbOutomaticSendEmail.Name = "cbOutomaticSendEmail";
            this.cbOutomaticSendEmail.Size = new System.Drawing.Size(155, 17);
            this.cbOutomaticSendEmail.TabIndex = 13;
            this.cbOutomaticSendEmail.Text = "Outomatic Send E-Mail";
            this.cbOutomaticSendEmail.UseVisualStyleBackColor = true;
            // 
            // btnIndent
            // 
            this.btnIndent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIndent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIndent.Image = global::LinkedIn_Applier.Properties.Resources.text_indent;
            this.btnIndent.Location = new System.Drawing.Point(387, 48);
            this.btnIndent.Name = "btnIndent";
            this.btnIndent.Size = new System.Drawing.Size(34, 23);
            this.btnIndent.TabIndex = 30;
            this.btnIndent.UseVisualStyleBackColor = true;
            this.btnIndent.Click += new System.EventHandler(this.BtnIndent_Click);
            // 
            // btnIndentRemove
            // 
            this.btnIndentRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIndentRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIndentRemove.Image = global::LinkedIn_Applier.Properties.Resources.text_indent_remove;
            this.btnIndentRemove.Location = new System.Drawing.Point(347, 48);
            this.btnIndentRemove.Name = "btnIndentRemove";
            this.btnIndentRemove.Size = new System.Drawing.Size(34, 23);
            this.btnIndentRemove.TabIndex = 29;
            this.btnIndentRemove.UseVisualStyleBackColor = true;
            this.btnIndentRemove.Click += new System.EventHandler(this.BtnIndentRemove_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.Enabled = false;
            this.btnRedo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnRedo.Image = global::LinkedIn_Applier.Properties.Resources.arrow_redo;
            this.btnRedo.Location = new System.Drawing.Point(143, 48);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(34, 23);
            this.btnRedo.TabIndex = 28;
            this.btnRedo.UseVisualStyleBackColor = true;
            this.btnRedo.Click += new System.EventHandler(this.BtnRedo_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Enabled = false;
            this.btnUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUndo.Image = global::LinkedIn_Applier.Properties.Resources.arrow_undo;
            this.btnUndo.Location = new System.Drawing.Point(103, 48);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(34, 23);
            this.btnUndo.TabIndex = 27;
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // btnSelectColor
            // 
            this.btnSelectColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSelectColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSelectColor.Image = global::LinkedIn_Applier.Properties.Resources.text_color;
            this.btnSelectColor.Location = new System.Drawing.Point(307, 48);
            this.btnSelectColor.Name = "btnSelectColor";
            this.btnSelectColor.Size = new System.Drawing.Size(34, 23);
            this.btnSelectColor.TabIndex = 26;
            this.btnSelectColor.UseVisualStyleBackColor = true;
            this.btnSelectColor.Click += new System.EventHandler(this.BtnSelectColor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Subject";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(60, 22);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(364, 20);
            this.txtSubject.TabIndex = 24;
            // 
            // btnUnderline
            // 
            this.btnUnderline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUnderline.Image = global::LinkedIn_Applier.Properties.Resources.text_underline;
            this.btnUnderline.Location = new System.Drawing.Point(266, 48);
            this.btnUnderline.Name = "btnUnderline";
            this.btnUnderline.Size = new System.Drawing.Size(34, 23);
            this.btnUnderline.TabIndex = 23;
            this.btnUnderline.UseVisualStyleBackColor = true;
            this.btnUnderline.Click += new System.EventHandler(this.BtnUnderline_Click);
            // 
            // btnItalic
            // 
            this.btnItalic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnItalic.Image = global::LinkedIn_Applier.Properties.Resources.text_italic;
            this.btnItalic.Location = new System.Drawing.Point(225, 48);
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Size = new System.Drawing.Size(34, 23);
            this.btnItalic.TabIndex = 22;
            this.btnItalic.UseVisualStyleBackColor = true;
            this.btnItalic.Click += new System.EventHandler(this.BtnItalic_Click);
            // 
            // btnBold
            // 
            this.btnBold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBold.Image = global::LinkedIn_Applier.Properties.Resources.text_bold;
            this.btnBold.Location = new System.Drawing.Point(184, 48);
            this.btnBold.Name = "btnBold";
            this.btnBold.Size = new System.Drawing.Size(34, 23);
            this.btnBold.TabIndex = 21;
            this.btnBold.UseVisualStyleBackColor = true;
            this.btnBold.Click += new System.EventHandler(this.BtnBold_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Mail Content";
            // 
            // lblAttachmentFile
            // 
            this.lblAttachmentFile.AutoSize = true;
            this.lblAttachmentFile.Location = new System.Drawing.Point(3, 331);
            this.lblAttachmentFile.Name = "lblAttachmentFile";
            this.lblAttachmentFile.Size = new System.Drawing.Size(95, 13);
            this.lblAttachmentFile.TabIndex = 19;
            this.lblAttachmentFile.Text = "Attachment File";
            // 
            // btmOpenFile
            // 
            this.btmOpenFile.Font = new System.Drawing.Font("Wide Latin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmOpenFile.Location = new System.Drawing.Point(269, 345);
            this.btmOpenFile.Name = "btmOpenFile";
            this.btmOpenFile.Size = new System.Drawing.Size(52, 23);
            this.btmOpenFile.TabIndex = 18;
            this.btmOpenFile.Text = "...";
            this.btmOpenFile.UseVisualStyleBackColor = true;
            this.btmOpenFile.Click += new System.EventHandler(this.BtmOpenFile_Click);
            // 
            // txtFileLocation
            // 
            this.txtFileLocation.Location = new System.Drawing.Point(6, 347);
            this.txtFileLocation.Name = "txtFileLocation";
            this.txtFileLocation.ReadOnly = true;
            this.txtFileLocation.Size = new System.Drawing.Size(257, 20);
            this.txtFileLocation.TabIndex = 17;
            // 
            // rtbMessage
            // 
            this.rtbMessage.Location = new System.Drawing.Point(6, 78);
            this.rtbMessage.Name = "rtbMessage";
            this.rtbMessage.Size = new System.Drawing.Size(418, 250);
            this.rtbMessage.TabIndex = 16;
            this.rtbMessage.Text = "";
            this.rtbMessage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RtbMessage_MouseDown);
            // 
            // btnSend
            // 
            this.btnSend.Font = new System.Drawing.Font("Wide Latin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.Location = new System.Drawing.Point(6, 370);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(418, 30);
            this.btnSend.TabIndex = 31;
            this.btnSend.Text = "SEND";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.BtnSend_Click);
            // 
            // grpEmailDetails
            // 
            this.grpEmailDetails.Controls.Add(this.btnSaveSettings);
            this.grpEmailDetails.Controls.Add(this.cbOutomaticSendEmail);
            this.grpEmailDetails.Controls.Add(this.btnSend);
            this.grpEmailDetails.Controls.Add(this.btnIndent);
            this.grpEmailDetails.Controls.Add(this.rtbMessage);
            this.grpEmailDetails.Controls.Add(this.btnIndentRemove);
            this.grpEmailDetails.Controls.Add(this.txtFileLocation);
            this.grpEmailDetails.Controls.Add(this.btnRedo);
            this.grpEmailDetails.Controls.Add(this.btmOpenFile);
            this.grpEmailDetails.Controls.Add(this.btnUndo);
            this.grpEmailDetails.Controls.Add(this.lblAttachmentFile);
            this.grpEmailDetails.Controls.Add(this.btnSelectColor);
            this.grpEmailDetails.Controls.Add(this.label4);
            this.grpEmailDetails.Controls.Add(this.label3);
            this.grpEmailDetails.Controls.Add(this.btnBold);
            this.grpEmailDetails.Controls.Add(this.txtSubject);
            this.grpEmailDetails.Controls.Add(this.btnItalic);
            this.grpEmailDetails.Controls.Add(this.btnUnderline);
            this.grpEmailDetails.Location = new System.Drawing.Point(1, 273);
            this.grpEmailDetails.Name = "grpEmailDetails";
            this.grpEmailDetails.Size = new System.Drawing.Size(435, 405);
            this.grpEmailDetails.TabIndex = 32;
            this.grpEmailDetails.TabStop = false;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSaveSettings.Location = new System.Drawing.Point(327, 345);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(97, 23);
            this.btnSaveSettings.TabIndex = 34;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = false;
            this.btnSaveSettings.Click += new System.EventHandler(this.BtnSaveSettings_Click);
            // 
            // cbHideEverything
            // 
            this.cbHideEverything.AutoSize = true;
            this.cbHideEverything.Location = new System.Drawing.Point(7, 216);
            this.cbHideEverything.Name = "cbHideEverything";
            this.cbHideEverything.Size = new System.Drawing.Size(207, 17);
            this.cbHideEverything.TabIndex = 33;
            this.cbHideEverything.Text = "Hide everything while searching";
            this.cbHideEverything.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 678);
            this.Controls.Add(this.cbHideEverything);
            this.Controls.Add(this.grpEmailDetails);
            this.Controls.Add(this.btnAddCountry);
            this.Controls.Add(this.btnDeleteCountry);
            this.Controls.Add(this.lstCountries);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtWorkType);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Linked In";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpEmailDetails.ResumeLayout(false);
            this.grpEmailDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtWorkType;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lstCountries;
        private System.Windows.Forms.Button btnDeleteCountry;
        private System.Windows.Forms.Button btnAddCountry;
        private System.Windows.Forms.CheckBox cbOutomaticSendEmail;
        private System.Windows.Forms.Button btnIndent;
        private System.Windows.Forms.Button btnIndentRemove;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnSelectColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Button btnUnderline;
        private System.Windows.Forms.Button btnItalic;
        private System.Windows.Forms.Button btnBold;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblAttachmentFile;
        private System.Windows.Forms.Button btmOpenFile;
        private System.Windows.Forms.TextBox txtFileLocation;
        private System.Windows.Forms.RichTextBox rtbMessage;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.GroupBox grpEmailDetails;
        private System.Windows.Forms.CheckBox cbHideEverything;
        private System.Windows.Forms.Button btnSaveSettings;
    }
}

