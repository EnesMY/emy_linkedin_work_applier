﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LinkedIn_Applier.DAL;
using Microsoft.Office.Interop.Outlook;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LinkedIn_Applier
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UpdateCountries();
        }
        string rtfLocation = "MailContent.rtf";
        IWebDriver ChromeBrowser;
        int MaxPageCount = 40;
        int CountryFounded = 0;
        string CurrentLocation = "";
        Dictionary<string, string> Emails = new Dictionary<string, string>();
        int CurrentIndex = 0;
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cbHideEverything.Checked) this.Hide();
            Counter = 0;
            CurrentIndex = 0;
            foreach (tblCountries item in lstCountries.Items)
            {
                try
                {
                    CurrentIndex++;
                    Logs.Write("NOW COUNTRY: " + item.CountryName + " INDEX " + CurrentIndex.ToString() + "/" + lstCountries.Items.Count.ToString());
                    CountryFounded = 0;
                    CurrentLocation = item.CountryName;
                    RunChrome(@"C:/NonTemp/", item.CountryName);
                    ReviewAllPage();
                }
                catch (System.Exception)
                {

                }

                CloseBrowser();
            }
            File.WriteAllText(DateTime.Now.ToBinary().ToString() + "Result.txt", Emails.ToDebugString());

            if (cbOutomaticSendEmail.Checked) SendEmailAllNew();
            if (cbHideEverything.Checked) this.Show();
        }

        void LookNewEmail()
        {
            System.Threading.Thread.Sleep(250);
            var Articles = ChromeBrowser.FindElements(By.ClassName("description"));
            IWebElement WorkDetails = Articles[0];
            string RegexPattern = @"([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}";

            Regex reg = new Regex(RegexPattern);
            string htmls = WorkDetails.GetAttribute("innerHTML");
            var Results = reg.Matches(htmls);
            foreach (Match Result in Results)
            {
                if (Result.Success)
                    AddEmail(Result.Value, ChromeBrowser.Url);
            }
        }
        int Counter = 0;
        void AddEmail(string EmailAdres, string WorkUrl)
        {
            string NewWorkUrl = GetWorkUrl(WorkUrl);
            if (!Emails.ContainsKey(EmailAdres))
            {
                using (dbMailEntities db = new dbMailEntities())
                    if (db.tblMail.Count(o => o.EMailAdress == EmailAdres) == 0)
                    {
                        db.tblMail.Add(

                                         new tblMail()
                                         {
                                             EMailAdress = EmailAdres,
                                             EmailSended = false,
                                             LinkedInURL = NewWorkUrl,
                                             Location = CurrentLocation
                                         });
                        db.SaveChanges();
                        Counter++;
                        CountryFounded++;
                    }

                Emails.Add(EmailAdres, WorkUrl);


                Logs.Write($"New E-Mail Count: {Counter}, Added E-Mail: { EmailAdres } and Work Url: { NewWorkUrl } This Counry Founded: {CountryFounded} {CurrentLocation}");
            }
        }

        private void ReviewAllPage()
        {
            System.Threading.Thread.Sleep(250);
            //First is outomatical opened!
            List<IWebElement> ULTags = ChromeBrowser.FindElements(By.ClassName("jobs-search-result-item")).Where(o => o.Displayed && o.Enabled).ToList();
            if (ULTags.Count == 0)
            {
                return;
            }
            foreach (IWebElement Work in ULTags)
            {
                if (Work.GetAttribute("itemtype") == "http://schema.org/ListItem")
                {

                    try
                    {
                        Work.Click();
                        LookNewEmail();
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(1000);
                    }

                }
            }
            NexPage();
        }

        int CurrentPage = 1;
        private void NexPage()
        {
            CurrentPage++;

            if (CurrentPage > MaxPageCount) { return; }

            string newUrl = NextPageUrl(ChromeBrowser.Url, CurrentPage);
            ChromeBrowser.Url = newUrl;
            Logs.Write("NEXT PAGE : " + CurrentPage.ToString() + "/" + MaxPageCount.ToString() + "  LOCATION : " + CurrentLocation + " - " + DateTime.Now.ToString());
            ReviewAllPage();
        }

        string GetWorkUrl(string Url)
        {
            string MainUrl = Url.Substring(0, Url.IndexOf('?'));
            var result = Url.Substring(Url.IndexOf('?') + 1).Split('&').ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
            string currentJobId = result.First(o => o.Key == "currentJobId").Value;
            return $"https://www.linkedin.com/jobs/view/{currentJobId}";
        }

        static string NextPageUrl(string Url, int Page)
        {
            string MainUrl = Url.Substring(0, Url.IndexOf('?'));
            var result = Url.Substring(Url.IndexOf('?') + 1).Split('&').ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
            string keywords = result.First(o => o.Key == "keywords").Value;
            string Location = result.First(o => o.Key == "location").Value;
            Page--;
            return $"https://www.linkedin.com/jobs/search/?keywords={keywords}&location={Location}&start={Page * 25}&sortBy=DD";
        }

        private void RunChrome(string CookieLocation, string Location)
        {
            if (ChromeBrowser != null) CloseBrowser();
            ChromeOptions ChromeOptions = new ChromeOptions();

            ChromeOptions.AddArguments("user-data-dir=" + CookieLocation);
            ChromeOptions.AddArgument("--start-maximized");
            ChromeOptions.AddArgument("--disable-plugins");
            ChromeOptions.AddArgument("no-sandbox");
            ChromeOptions.AddArgument("disable-gpu");
            if (cbHideEverything.Checked)
            {
                ChromeOptions.AddArgument("headless");
            }


            ChromeDriverService ChromeService = ChromeDriverService.CreateDefaultService();
            ChromeService.HideCommandPromptWindow = true;
            ChromeBrowser = new ChromeDriver(ChromeService, ChromeOptions);



            ChromeBrowser.Manage().Cookies.DeleteAllCookies();

            ChromeBrowser.Url = "https://www.linkedin.com/jobs/";

            System.Threading.Thread.Sleep(2000);
            //OLD SYSTEM
            //var DisabledNesne = ChromeBrowser.FindElements(By.TagName("artdeco-typeahead-deprecated-input"));
            //IWebElement SearchBox = DisabledNesne[0];
            //IWebElement LocationBox = DisabledNesne[1];

            //SearchBox.FindElements(By.TagName("input"))[1].SendKeys(txtWorkType.Text);
            //LocationBox.FindElements(By.TagName("input"))[1].SendKeys(Location);
            //System.Threading.Thread.Sleep(500);
            //IWebElement SendButton = ChromeBrowser.FindElements(By.ClassName("jobs-search-box__submit-button")).FirstOrDefault();
            //OLD SYSTEM

            IWebElement SearchBox = ChromeBrowser.FindElement(by: By.Id("keyword-box-input"));
            if (SearchBox == null) return;
            SearchBox.Clear();
            SearchBox.SendKeys(txtWorkType.Text);

            IWebElement LocationBox = ChromeBrowser.FindElement(by: By.Id("location-box-input"));
            if (LocationBox == null) return;
            LocationBox.Clear();
            LocationBox.SendKeys(Location);
            System.Threading.Thread.Sleep(500);


            LocationBox.Submit();
            System.Threading.Thread.Sleep(500);

            LocationBox = ChromeBrowser.FindElement(by: By.Id("location-box-input"));
            LocationBox.Clear();

            LocationBox.SendKeys(Location);
            System.Threading.Thread.Sleep(500);
            LocationBox.Submit();

            System.Threading.Thread.Sleep(2000);
            var BeforeSearchResult = ChromeBrowser.FindElements(by: By.ClassName("pagination__pages"));
            if (BeforeSearchResult.Count == 0) return;


            var SearchResult = BeforeSearchResult.FirstOrDefault().FindElements(by: By.TagName("li")).ToList();
            if (SearchResult.Count == 0) return;
            string Result = "";
            foreach (var webElement in SearchResult)
            {
                Result = webElement.Text;
            }
            MaxPageCount = Convert.ToInt32(Result);
            CurrentPage = 1;
            string newUrl = NextPageUrl(ChromeBrowser.Url, CurrentPage);
            ChromeBrowser.Url = newUrl;
        }

        private void CloseBrowser()
        {
            if (ChromeBrowser != null)
            {
                try
                {
                    var tabs = ChromeBrowser.WindowHandles;
                    tabs.ToList().ForEach(o => ChromeBrowser.SwitchTo().Window(o).Close());
                    ChromeBrowser.Quit();
                }
                catch (WebDriverException)
                {

                }
                catch (System.Exception)
                {

                }
                finally
                {
                    ChromeBrowser.Dispose();
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseBrowser();
        }

        private void btnAddCountry_Click(object sender, EventArgs e)
        {
            using (dbMailEntities DB = new dbMailEntities())
            {
                int RowCount = DB.tblCountries.Count(o => o.CountryName == txtLocation.Text);
                if (RowCount == 0)
                {
                    DB.tblCountries.Add(new tblCountries() { CountryName = txtLocation.Text });
                    DB.SaveChanges();
                    txtLocation.Text = "";
                    txtLocation.Focus();
                    ActiveControl = txtLocation;
                }
            }

            UpdateCountries();
        }

        private void UpdateCountries()
        {
            using (dbMailEntities DB = new dbMailEntities())
            {
                lstCountries.DataSource = DB.tblCountries.AsNoTracking().OrderBy(o => o.CountryName).ToList();
                lstCountries.DisplayMember = "CountryName";
                lstCountries.ValueMember = "ID";
            }

        }

        private void BtnDeleteCountry_Click(object sender, EventArgs e)
        {
            if (lstCountries.SelectedIndex > -1)
            {
                using (dbMailEntities DB = new dbMailEntities())
                {
                    var Country = DB.tblCountries.FirstOrDefault(o => o.ID == ((tblCountries)lstCountries.SelectedItem).ID);
                    if (Country == null)
                    {
                        DB.Database.Connection.Close();
                        DB.Dispose();
                        return;
                    }
                    DB.tblCountries.Remove(Country);
                    DB.SaveChanges();
                    UpdateCountries();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSettings();
            Logs.LogFileName = "Logs/logs_" + DateTime.Now.ToBinary().ToString() + ".xml";
        }

        private string GetSignatureLocation()
        {
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
            string signatureLocation = string.Empty;
            DirectoryInfo diInfo = new DirectoryInfo(appDataDir);

            if (diInfo.Exists)
            {
                FileInfo[] fiSignature = diInfo.GetFiles("*.rtf");

                if (fiSignature.Length > 0)
                {
                    signatureLocation = fiSignature[0].FullName;
                }
            }
            return signatureLocation;
        }

        void SaveSettings()
        {

            using (dbMailEntities db = new dbMailEntities())
            {
                if (db.tblSettings.Count() == 0)
                {
                    tblSetting setting = new tblSetting();
                    SetSettingDetails(setting);
                    db.tblSettings.Add(setting);
                }
                else
                {
                    tblSetting setting = db.tblSettings.First();
                    SetSettingDetails(setting);
                }
                db.SaveChanges();
            }




            rtbMessage.SaveFile(rtfLocation);
            MessageBox.Show("Saved!");
        }

        private void SetSettingDetails(tblSetting setting)
        {
            setting.AutomaticSendMail = cbOutomaticSendEmail.Checked;
            setting.CVLocation = txtFileLocation.Text;
            setting.HideEverythingWhileSearching = cbHideEverything.Checked;
            setting.WorkKeyword = txtWorkType.Text;
            setting.MailSubject = txtSubject.Text;
        }

        void LoadSettings()
        {
            using (dbMailEntities db = new dbMailEntities())
            {
                if (db.tblSettings.Count() > 0)
                {

                    tblSetting setting = db.tblSettings.First();
                    cbOutomaticSendEmail.Checked = (bool)setting.AutomaticSendMail;
                    cbHideEverything.Checked = (bool)setting.HideEverythingWhileSearching;
                    txtSubject.Text = setting.MailSubject;
                    txtFileLocation.Text = setting.CVLocation;
                    txtWorkType.Text = setting.WorkKeyword;
                }
            }

            if (File.Exists(rtfLocation))
                rtbMessage.LoadFile(rtfLocation);
            else
            {
                string SignLocation = GetSignatureLocation();
                if (!string.IsNullOrEmpty(SignLocation))
                    rtbMessage.LoadFile(SignLocation);
            }
        }

        void SendEmailAllNew()
        {

            using (dbMailEntities DB = new dbMailEntities())
            {
                var Result = DB.tblMail.Where(o => o.EmailSended != true).ToList();
                foreach (tblMail Mail in Result)
                {
                    SendEmail(Mail.EMailAdress, Mail.LinkedInURL, Mail.Location);
                    Mail.EmailSended = true;
                }
                DB.SaveChanges();
            }
        }

        void SendEmail(string EmailAdress, string WorkUrl, string Location)
        {
            Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
            //Create the new message by using the simplest approach.
            MailItem oMsg = (MailItem)oApp.CreateItem(OlItemType.olMailItem);

            //Add a recipient.
            // TODO: Change the following recipient where appropriate.
            Recipient oRecip = oMsg.Recipients.Add(EmailAdress);
            oRecip.Resolve();

            //Set the basic properties.
            oMsg.Subject = ApplyParameters(txtSubject.Text, WorkUrl, Location);
            oMsg.Body = ApplyParameters(rtbMessage.Rtf, WorkUrl, Location);
            Attachment oAttach;

            //Add an attachment.
            // TODO: change file path where appropriate
            String sSource = txtFileLocation.Text;
            if (!string.IsNullOrEmpty(sSource))
            {
                String sDisplayName = "Resume File";
                int iPosition = (int)oMsg.Body.Length + 1;
                int iAttachType = (int)OlAttachmentType.olByValue;
                oAttach = oMsg.Attachments.Add(sSource, iAttachType, iPosition, sDisplayName);
            }

            oMsg.DeleteAfterSubmit = false;

            // If you want to, display the message.
            // oMsg.Display(true);  //modal

            //Send the message.
            oMsg.Save();
            oMsg.Send();

            //Explicitly release objects.
            oRecip = null;
            oAttach = null;
            oMsg = null;
            oApp = null;
        }

        string ApplyParameters(string MainString, string WorkUrl, string Location)
        {
            return MainString.Replace("@WorkUrl", WorkUrl).Replace("@Location", Location);
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            rtbMessage.Undo();
            UndoRedoSet();
        }

        void UndoRedoSet()
        {
            btnUndo.Enabled = rtbMessage.CanUndo;
            btnRedo.Enabled = rtbMessage.CanRedo;
        }

        private void BtnRedo_Click(object sender, EventArgs e)
        {
            rtbMessage.Redo();
            UndoRedoSet();
        }

        private void RtbMessage_MouseDown(object sender, MouseEventArgs e)
        {
            UndoRedoSet();
        }

        private void BtnBold_Click(object sender, EventArgs e)
        {
            SetFontStyle(FontStyle.Bold);
        }

        private void BtmOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "PDF (.pdf)|*.pdf|Text File (.txt)|*.txt|Word File (.docx ,.doc)|*.docx;*.doc|Spreadsheet (.xls ,.xlsx)|  *.xls ;*.xlsx|Presentation (.pptx ,.ppt)|*.pptx;*.ppt";
            opf.Title = "Attachment!";
            opf.Multiselect = false;
            DialogResult dr = opf.ShowDialog();
            if (dr == DialogResult.OK && opf.CheckFileExists)
            {
                txtFileLocation.Text = opf.FileName;
            }
        }

        void SetFontStyle(FontStyle fs)
        {
            Font fnt = rtbMessage.SelectionFont;
            if (fnt.Style.HasFlag(fs))
            {
                rtbMessage.SelectionFont = new Font(fnt.FontFamily, fnt.Size, fnt.Style & ~fs);
            }
            else
            {
                rtbMessage.SelectionFont = new Font(fnt.FontFamily, fnt.Size, fnt.Style | fs);
            }

        }

        private void BtnItalic_Click(object sender, EventArgs e)
        {
            SetFontStyle(FontStyle.Italic);
        }

        private void BtnUnderline_Click(object sender, EventArgs e)
        {
            SetFontStyle(FontStyle.Underline);
        }

        private void BtnSelectColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            DialogResult dr = cd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                rtbMessage.SelectionColor = cd.Color;
            }
        }

        private void BtnIndentRemove_Click(object sender, EventArgs e)
        {
            if (rtbMessage.SelectionIndent > 5)
                rtbMessage.SelectionIndent -= 5;
            else
            {
                rtbMessage.SelectionIndent = 0;
            }
        }

        private void BtnIndent_Click(object sender, EventArgs e)
        {
            rtbMessage.SelectionIndent += 5;
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            SendEmailAllNew();
        }

        private void BtnSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }
    }
}
