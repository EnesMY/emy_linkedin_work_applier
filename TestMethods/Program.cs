﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            string LinkUrl = "https://www.linkedin.com/jobs/search/?keywords=C%23&location=United%20States&locationId=us%3A0&start=25";
            Console.WriteLine(NextPageUrl(LinkUrl, 5)); 
        }

        static string StringCompare(string MainUrl, Dictionary<string, string> Parameters)
        {
            bool FirstKey = true;
            foreach (KeyValuePair<string, string> item in Parameters)
            {
                if (FirstKey)
                {
                    MainUrl += "?";
                    FirstKey = false;
                }
                else MainUrl += "&";
                MainUrl += item.Key + "=" + item.Value;
            }
            return MainUrl;
        }


        static string NextPageUrl(string Url,int Page)
        {
            string MainUrl = Url.Substring(0, Url.IndexOf('?'));
            var result = Url.Substring(Url.IndexOf('?') + 1).Split('&').ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
            string keywords = result.First(o => o.Key == "keywords").Value;
            string Location = result.First(o => o.Key == "location").Value;
            string locationID = result.First(o => o.Key == "locationId").Value;
            Page--;
            return $"https://www.linkedin.com/jobs/search/?keywords={keywords}&location={Location}&locationId={locationID}&start={Page * 25}";
        }

        
    }
}
